class User < ActiveRecord::Base
validates :first_name,  :presence => { :message => "you must filled first name" },
	  :format => {:with => /\A[a-zA-Z\s]+$\Z/}, 
          :length => {:minimum => 1, :maximum => 25};

validates :last_name,  :presence => { :message => "you must filled last name" },
	  :format => {:with => /\A[a-zA-Z\s]+$\Z/}, 
          :length => {:minimum => 1, :maximum => 25};

validates :email, :presence => { :message => "you must filled email" },
                    :length => {:minimum => 3, :maximum => 254},
                    :uniqueness => { :message => "email already exist by another user" },
                    :format => {:with => /\A^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$\Z/i};

validates :username, :presence => { :message => "you must filled username" };
validates :date_of_birth, :presence => { :message => "you must filled date of birth" };
validates :password, :presence => { :message => "you must filled password" };
validates :age, :presence => { :message => "you must filled age" };
validates :address, :presence => { :message => "you must filled address" };
validates :country_id, :presence => { :message => "you must filled country id" };
	attr_accessible :first_name, :last_name, :email, :username, :password, :date_of_birth, :age, :address, :country_id
	
               
  	

has_many :product,  :dependent => :destroy 
has_many :article,  :dependent => :destroy
has_many :article_my,
	 :class_name => "Article",
	 :foreign_key => "user_id",
	 :conditions => "title like '%my country%'";
belongs_to :country
has_many :category,  :dependent => :destroy 
has_many :comments,  :dependent => :destroy
scope :ind, where("country_id = '1'")

 def self.age
	where("age > 18")
 end

 def full_address
	
	"#{self.address} #{self.country.name}"
 end
end
