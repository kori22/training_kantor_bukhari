class Comment < ActiveRecord::Base
	validates :content, :presence => { :message => "you must filled content" }; 
	validates :user_id, :presence => { :message => "you must filled user id" };
	validates :article_id, :presence => { :message => "you must filled article id" };  
	attr_accessible :content, :user_id, :article_id
	belongs_to :user
	belongs_to :article
end
