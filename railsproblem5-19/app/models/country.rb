class Country < ActiveRecord::Base
	validates :code,  :presence => { :message => "you must filled code of country" },
	  :format => {:with => /\A[a-zA-Z\s]+$\Z/}, 
          :length => {:minimum => 1, :maximum => 3};
	
	validates :name, :presence => { :message => "you must filled name of country" }; 
	attr_accessible :code, :name
	has_one :user
	has_many :user_my,
	 
	 :class_name => "User",
	 :foreign_key => "country_id",
	 :conditions => "country_id = '1'";
end
