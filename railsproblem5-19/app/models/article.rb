class Article < ActiveRecord::Base
	validates :title,  :presence => { :message => "you must filled title" },
	  :uniqueness => true,
          :allow_blank => false,
	  :allow_nil => false;
	validates :descriptions, :presence => { :message => "you must filled descriptions" }; 
	validates :rating, :presence => { :message => "you must filled rating" }; 
	validates :user_id, :presence => { :message => "you must filled user id" }; 
	attr_accessible :descriptions, :title, :rating, :user_id
	belongs_to :user
	has_many :comment,  :dependent => :destroy
	scope :rating, lambda {|bar| where("rating > ?", bar)}
	def self.content
	where("character_length(descriptions) > 100")
	end
end
