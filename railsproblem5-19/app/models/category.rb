class Category < ActiveRecord::Base
	validates :name, :presence => { :message => "you must filled category name" };
	validates :user_id, :presence => { :message => "you must filled user id" };  
	attr_accessible :name, :user_id
	belongs_to :user
	has_many :products, :through => :products_categories
	has_many :products_categories
	has_many :product_my,
	 :class_name => "Product",
	 :foreign_key => "category_id",
	 :conditions => "products.name like '%shoes%'";
	scope :book, where("name like '%books%'")
end
