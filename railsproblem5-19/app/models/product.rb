class Product < ActiveRecord::Base
	validates :price,  :presence => true,
          :numericality => true,
	  :format => {:with => /\A[0-9\s]+$\Z/};
	validates :name, :presence => { :message => "you must filled name of product" }; 
	validates :stock, :presence => { :message => "you must filled total stock" }; 
	validates :description, :presence => { :message => "you must filled description" };
	validates :user_id, :presence => { :message => "you must filled user id" };
	validates :category_id, :presence => { :message => "you must filled category id" };   
	attr_accessible :name, :price, :stock, :description, :user_id, :category_id
	belongs_to :user
	has_many :categories, :through => :products_categories
	has_many :products_categories
	scope :stock, lambda {|bar| where("stock > ?", bar)}
	scope :price, where("price < 1000")
end
