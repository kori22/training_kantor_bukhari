# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

data_users = [
  [ 'irman', 'taufik', 'taufik@rr', 'taufik', 'rty', '10-10-10', '43', 'tasik'],
  [ 'azmi', 'hakim', 'huk@rr', 'azmi', 'rty', '10-10-45', '21', 'medan'],
  [ 'angga', 'setiawan', 'aa@rr', 'angga', 'rty', '14-10-15', '8', 'bandung'],
  [ 'ari', 'aldiyan', 'ari@rr', 'ari', 'rfg', '13-13-13', '13', 'bandung'],
  [ 'kori', 'riko', 'kori@rr', 'kori', 'rty', '10-11-10', '33', 'padang']
]

data_articles = [
  [ 'php', 'test test'],
  [ 'ci', 'test test'],
  [ 'zend', 'test test'],
  [ 'cake', 'test test'],
  [ 'ruby', 'test test']
  
]

data_comments = [
  [ 'baru 1'],
  [ 'baru 2'],
  [ 'baru 3'],
  [ 'baru 4'],
  [ 'baru 5']
]

data_countries = [
  [ 'ind', 'indonesia'],
  [ 'arb', 'arab saudi'],
  [ 'eng', 'inggris'],
  [ 'us', 'amerika serikat'],
  [ 'cnd', 'canada']
  
]



data_users.each do |first_name, last_name, email, username, password, date_of_birth, age, address|
User.create( first_name: first_name, last_name: last_name, email: email, username: username, password: password, date_of_birth: date_of_birth, age: age, address: address)
end

data_articles.each do |title, body|
Article.create( title: title, body: body)
end

data_comments.each do |content|
Comment.create( content: content)
end

data_countries.each do |code, name|
Country.create( code: code, name: name)
end
