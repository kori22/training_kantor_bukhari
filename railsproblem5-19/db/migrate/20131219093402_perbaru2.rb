class Perbaru2 < ActiveRecord::Migration
  def change
	add_column :articles, :rating, :string
	change_column :users, :address, :text
	rename_column :articles, :body, :descriptions
  end
end
