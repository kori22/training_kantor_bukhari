class CreateProductsCategories < ActiveRecord::Migration
  def self.up
    create_table :products_categories, :id => false do |t|
	t.string :product_id
	t.string :category_id
    end
	add_index :products_categories, [:product_id, :category_id]
  end

  def self.down
    drop_table :products_categories
  end
end
