class Perbaru3 < ActiveRecord::Migration
  def change
	add_column :products, :user_id, :int
	add_column :articles, :user_id, :int
	add_column :users, :country_id, :int
	add_column :comments, :user_id, :int
	add_column :comments, :article_id, :int
	add_column :categories, :user_id, :int
	add_column :products, :category_id, :int
  end
end
